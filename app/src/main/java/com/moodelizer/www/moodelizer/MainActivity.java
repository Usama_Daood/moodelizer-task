package com.moodelizer.www.moodelizer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text=findViewById(R.id.touch);
        final TextView xCoord = findViewById(R.id.x_axis);
        final TextView yCoord = findViewById(R.id.y_axis);

        text.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    // Offsets are for centering the TextView on the touch location
                    v.setX(event.getRawX());
                    v.setY(event.getRawY());
                    xCoord.setText("X Axis "+String.valueOf((int) event.getX()));
                    yCoord.setText("Y Axis "+String.valueOf((int) event.getY()));
                }

                return true;
            }

        });
    }
}
